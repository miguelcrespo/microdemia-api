<?php
/**
 * Created by PhpStorm.
 * User: ESTUDIANTE
 * Date: 27/03/14
 * Time: 04:26 PM
 */
class Tutor extends Eloquent{
    protected $table = "tutores";
    protected $fillable = array('usuarios_id');
    public function tutorias(){
        return $this->hasMany('Tutoria', 'tutores_id');
    }
    public function usuario(){
        return $this->belongsTo('Usuario', 'usuarios_id');
    }
    public function asignaturas(){
        return $this->belongsToMany('Asignatura', 'tutores_asignaturas', 'tutores_id', 'asignaturas_id');
    }


}