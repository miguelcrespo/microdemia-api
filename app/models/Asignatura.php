<?php
/**
 * Created by PhpStorm.
 * User: ESTUDIANTE
 * Date: 27/03/14
 * Time: 05:17 PM
 */
class Asignatura extends Eloquent{
    protected $table = "asignaturas";
    protected $fillable = array('nombre', 'descripcion');
    public function tutorias(){
        return $this->hasMany('Tutoria');
    }
    public function tutores(){
        return $this->belongsToMany('Tutor', 'tutores_asignaturas', 'asignaturas_id', 'tutores_id');
    }
     public function usuarios(){
        return $this->belongsToMany('Usuario', 'usuarios_asignaturas', 'asignaturas_id', 'usuarios_id');
    }
    public function carreras(){
        return $this->belongsToMany('Carrera', 'carreras_asignaturas', 'asignaturas_id', 'carreras_id');
    }

}