<?php
/**
 * Created by PhpStorm.
 * User: ESTUDIANTE
 * Date: 27/03/14
 * Time: 05:59 PM
 */
class Tutoria extends Eloquent{
    protected $table = "tutorias";
    protected $fillable = array('nombre', 'descripcion', 'fecha', 'lugar', 'capacidad', 'tutores_id', 'asignaturas_id');

    public function tutor(){
        return $this->belongsTo('Tutor', 'tutores_id');
    }
    public function asignatura(){
        return $this->belongsTo('Asignatura', 'asignaturas_id');
    }
    public function usuarios(){
        return $this->belongsToMany('Usuario', 'tutorias_usuarios', 'tutorias_id', 'usuarios_id')->withPivot('calificacion', 'created_at')->withTimestamps();;
    }

}