<?php
/**
 * Created by Miguel Crespo.
 * Date: 26/03/14
 * Time: 04:39 PM
 */
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Usuario extends Eloquent implements UserInterface, RemindableInterface{

    protected $table = "usuarios";
    protected $hidden = array('password');
    protected $fillable = array('nombre', 'correo', 'password', 'carreras_id', 'descripcion');

    public function getAuthIdentifier(){
        return $this->getKey();
    }
    public function getAuthPassword(){
        return $this->password;
    }
    public function getReminderEmail(){
        return $this->email;
    }
    public function tutor(){
        return $this->hasOne('Tutor', 'usuarios_id');
    }

    public function tutorias(){
        return $this->belongsToMany('Tutoria', 'tutorias_usuarios', 'usuarios_id', 'tutorias_id')->withTimestamps();;
    }

    public function carrera(){
        return $this->belongsTo('Carrera', 'carreras_id');
    }
    
    public function asignaturas(){
        return $this->belongsToMany('Asignatura', 'usuarios_asignaturas', 'usuarios_id', 'asignaturas_id');
    }
}
