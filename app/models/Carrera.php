<?php
/**
 * Created by PhpStorm.
 * User: Miguel
 * Date: 5/5/14
 * Time: 11:41 PM
 */

class Carrera extends Eloquent{
    protected $table = "carreras";
    protected $fillable = array('nombre');
    public function usuario(){
        return $this->hasMany('Usuario', 'usuarios_id');
    }
    public function asignaturas(){
        return $this->belongsToMany('Asignatura', 'carreras_asignaturas', 'carreras_id', 'asignaturas_id');
    }


}