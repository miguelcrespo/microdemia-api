<?php

/**
 * Created by PhpStorm.
 * User: ESTUDIANTE
 * Date: 27/03/14
 * Time: 07:19 PM
 */
class TutoriaController extends BaseController
{
    public function todasTutorias()
    {
        return Tutoria::All();
    }


    public function getTutorials(){
        $responsecontroller = new ResponseController(microtime(true));
        if(count(Auth::user()->asignaturas)>0){
            $date = date('Y-m-d H:i:s');
            $tutorialiwant = Auth::user()->asignaturas;
            $filtertutorial = [];
            foreach ($tutorialiwant as $tuto) {
               // echo($tuto->id."<br>");
                array_push($filtertutorial, $tuto->id);
            }

            //print_r($filtertutorial);
            $tutorials = Tutoria::where("fecha", ">", $date)->where("estado", "=", 1)->whereIn('asignaturas_id', $filtertutorial)->get();

            $tutorialsarray = [];
            $enrolled = false;
            foreach ($tutorials as $tutorial) {
                // Charge assistants
                $assistants = [];
                foreach ($tutorial->usuarios as $user) {
                    if ($user->id == Auth::user()->id) {
                        $enrolled = true;
                    }
                    $user2 = [
                        "id" => $user->id,
                        "name" => $user->name
                    ];
                    array_push($assistants, $user2);
                }

                $tuto = [
                    "id" => $tutorial->id,
                    "name" => $tutorial->nombre,
                    "description" => $tutorial->descripcion,
                    //"created_at" => $tutorial->created_at,
                    // "updated_at" => $tutorial->updated_at,
                    "date" => $tutorial->fecha,
                    "place" => $tutorial->lugar,
                    "capacity" => $tutorial->capacidad,
                    "tutors_id" => $tutorial->tutores_id,
                    "subjects_id" => $tutorial->asignaturas_id,
                    "state" => $tutorial->estado,
                    "subjects_name" => $tutorial->asignatura->nombre,
                    "tutor_name" => $tutorial->tutor->usuario->nombre,
                    "assistants" => $assistants,
                    "users_id" => $tutorial->tutor->usuario->id,
                    "enrolled" => $enrolled
                ];
                array_push($tutorialsarray, $tuto);
            }
            $responsecontroller->setError(false);
            $responsecontroller->setData($tutorialsarray);
        }else{
            $responsecontroller->setError(true);
            $responsecontroller->setDescripcion("El usuario no ha especificado asignaturas");
        }
        return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));

    }

    public function findTutorial()
    {
        $id = Input::get('id');
        $tutorial = Tutoria::find($id);
        $assistants = [];
        $enrolled = false;
        $yourrate =0 ;
        $rate = 0;
        $votes = 0;
        foreach ($tutorial->usuarios as $user) {
            // we calculate average
            if($tutorial->estado == 0 && $user-> pivot->calificacion!=0){
                $rate += $user-> pivot->calificacion;
                $votes ++;
            }
            if ($user->id == Auth::user()->id) {
                $enrolled = true;
                $yourrate = $user-> pivot->calificacion;
            }
            $user2 = [
                "id" => $user->id,
                "name" => $user->nombre
            ];
            array_push($assistants, $user2);
        }

        $tuto = [
            "id" => $tutorial->id,
            "name" => $tutorial->nombre,
            "description" => $tutorial->descripcion,
            //"created_at" => $tutorial->created_at,
            // "updated_at" => $tutorial->updated_at,
            "date" => $tutorial->fecha,
            "place" => $tutorial->lugar,
            "capacity" => $tutorial->capacidad,
            "tutors_id" => $tutorial->tutores_id,
            "subjects_id" => $tutorial->asignaturas_id,
            "state" => $tutorial->estado,
            "subjects_name" => $tutorial->asignatura->nombre,
            "tutor_name" => $tutorial->tutor->usuario->nombre,
            "assistants" => $assistants,
            "users_id" => $tutorial->tutor->usuario->id,
            "enrolled" => $enrolled
        ];
        if($enrolled){
            $tuto["yourrate"] = $yourrate;
           // array_push($tuto,  $rate);
        }
        if($tutorial->estado == 0){

            $tuto["rate"] = [
                "rate"=> $rate/$votes,
                "votes" => $votes
            ];
        }
        return Response::json($tuto)->setCallback(Input::get('callback'));
    }

    public function disabledTutorials(){
        $date = date('Y-m-d H:i:s');
        $tutorials = Tutoria::where("fecha", "<", $date)->where("estado", "=", "1")->get();
        foreach ($tutorials as $tutorial) {
            $tutorial->estado = 0; // disable tutorial
            $tutorial->save();
        }

        return Response::json($tutorials)->setCallback(Input::get('callback'));
    }

    public function getTutorialsBySubject(){
        $id = Input::get('id');
        $responsecontroller = new ResponseController(microtime(true));
        $tutorialsarray = [];
        if(count(Auth::user()->asignaturas)>0){
            $date = date('Y-m-d H:i:s');
            //print_r($filtertutorial);
           // $tutorials = Tutoria::where("fecha", ">", $date)->where("estado", "=", 1)->whereIn('asignaturas_id', $filtertutorial)->get();
            $tutorials = Tutoria::where("asignaturas_id", "=",  $id)->where("fecha", ">", $date)->get();

            $enrolled = false;
            foreach ($tutorials as $tutorial) {
                // Charge assistants
                $assistants = [];
                foreach ($tutorial->usuarios as $user) {
                    if ($user->id == Auth::user()->id) {
                        $enrolled = true;
                    }
                    $user2 = [
                        "id" => $user->id,
                        "name" => $user->name
                    ];
                    array_push($assistants, $user2);
                }

                $tuto = [
                    "id" => $tutorial->id,
                    "name" => $tutorial->nombre,
                    "description" => $tutorial->descripcion,
                    //"created_at" => $tutorial->created_at,
                    // "updated_at" => $tutorial->updated_at,
                    "date" => $tutorial->fecha,
                    "place" => $tutorial->lugar,
                    "capacity" => $tutorial->capacidad,
                    "tutors_id" => $tutorial->tutores_id,
                    "subjects_id" => $tutorial->asignaturas_id,
                    "state" => $tutorial->estado,
                    "subjects_name" => $tutorial->asignatura->nombre,
                    "tutor_name" => $tutorial->tutor->usuario->nombre,
                    "assistants" => $assistants,
                    "users_id" => $tutorial->tutor->usuario->id,
                    "enrolled" => $enrolled
                ];
                array_push($tutorialsarray, $tuto);
            }
            $responsecontroller->setError(false);
            $responsecontroller->setData($tutorialsarray);
        }else{
            $responsecontroller->setError(true);
            $responsecontroller->setDescripcion("El usuario no ha especificado asignaturas");
        }
        //return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));


        return Response::json($tutorialsarray)->setCallback(Input::get('callback'));

    }

}