<?php
class ResponseController{

	protected $data;
	protected $descripcion;
	protected $error = false;
    protected $fechainicial;

    public function ResponseController($fecha = null){
        $this->fechainicial = $fecha;
    }
	public function setFechaInicial($fechainicial){
		$this->fechainicial = $fechainicial;
	}

	public function setError($error){
		$this->error = $error;
	}

	public function setDescripcion($descripcion){
		$this->descripcion = $descripcion;
	}

	public function setData($data){
		$this->data = $data;
	}


	public function getResponse(){
		$response = [
			"tiempo_procesamiento" => microtime(true)-$this->fechainicial,
			"error" => $this->error,
			"descripcion" => $this->descripcion,
			"data" => $this->data

		];
		return $response;
	}
}

?>