<?php

/**
 * Created by PhpStorm.
 * User: ESTUDIANTE
 * Date: 28/03/14
 * Time: 18:09
 */
class TutorController extends BaseController
{
    public function todasmisTutorias()
    {
        $arraytutorias = [];
        $tutorias = Auth::user()->tutor->tutorias;
        foreach ($tutorias as $tutoria) {
            $tuto = [
                "id" => $tutoria->id,
                "nombre" => $tutoria->nombre,
                "descripcion" => $tutoria->descripcion,
                "fecha" => $tutoria->fecha,
                "lugar" => $tutoria->lugar,
                "capacidad" => $tutoria->capacidad,
                "tutor" => $tutoria->tutores_id,
                "asignatura" => $tutoria->asignaturas_id
            ];
            array_push($arraytutorias, $tuto);
        }
        return json_encode($arraytutorias);
    }


    public function agregarAsignatura($id, $idasignatura)
    {
        $usuario = Usuario::find($id);
        if (!is_null($usuario->tutor)) {
            $usuario->tutor->asignaturas()->attach($idasignatura);
            return true;
        } else {
            $input = array(
                'usuarios_id' => Auth::user()->id,
            );

            Tutor::create($input);
            echo $usuario->tutor->id;
            //$usuario->tutor->asignaturas()->attach($idasignatura);
            //return true;
        }
    }

    public function crearTutor()
    {
        $input = array(
            'usuarios_id' => Auth::user()->id,
        );

        Tutor::create($input);
    }

    public function misAsignaturas()
    {
        $arrayasignaturas = [];
        if (is_object(Auth::user()->tutor)) {

            $asignaturas = Auth::user()->tutor->asignaturas;
            foreach ($asignaturas as $asig) {
                $asignatura = [
                    "id" => $asig->id,
                    "nombre" => $asig->nombre
                ];
                array_push($arrayasignaturas, $asignatura);

            }
            //return json_encode($arrayasignaturas);
        }
        return Response::json($arrayasignaturas)->setCallback(Input::get('callback'));

    }

    public function soyTutor()
    { // Comprueba si el usuario es un tutor
        $user = Auth::user();
        if (!empty($user->tutor->id)) {
            return "true";
        } else {
            return "false";
        }
    }

    public function crearArrayTutoria($tutoria)
    {
        return $tuto = [
            "id" => $tutoria->id,
            "nombre" => $tutoria->nombre,
            "descripcion" => $tutoria->descripcion,
            "fecha" => $tutoria->fecha,
            "lugar" => $tutoria->lugar,
            "capacidad" => $tutoria->capacidad,
            "tutor" => $tutoria->tutor,
            "asignaturas_id" => $tutoria->asignaturas_id
        ];
    }

    public function misTutoriasPendientes()
    {
        $arraytutorias = [];
        if (!is_null(Auth::user()->tutor)) {
            $tutorias = Auth::user()->tutor->tutorias;
            $fechaactual = date('Y-m-d H:i:s');

            foreach ($tutorias as $tutoria) {
                if (strtotime($fechaactual) < strtotime($tutoria->fecha)) { // obtenemos las tutorias cuya fecha sea superior a la fecha actual
                    array_push($arraytutorias, $tutoria);
                }
            }
        }
        return $arraytutorias;
    }

    public function misTutoriasRealizadas()
    {
        $arraytutorias = [];
        if (!is_null(Auth::user()->tutor)) {
            $tutorias = Auth::user()->tutor->tutorias;
            $fechaactual = date('Y-m-d H:i:s');

            foreach ($tutorias as $tutoria) {
                if (strtotime($fechaactual) > strtotime($tutoria->fecha)) {
                    array_push($arraytutorias, $tutoria);
                }
            }
        }
        return $arraytutorias;
    }

    public function prueba($name)
    {
        $response = new ResponseController(microtime(true));
        sleep(2);
        return $response->getResponse();
    }

    public function crearTutoria()
    {
        $response = new ResponseController();
        $response->setFechaInicial(microtime(true));
        $duracion = Input::get('hour');
        $hora = strtotime($duracion);
        $hora = date("H:i:s", $hora);
        $date = DateTime::createFromFormat('d/m/Y H:i:s', Input::get('date') . " " . $hora);
        $fecha = $date->format('Y-m-d H:i:s');
        $fechaactual = date('Y-m-d H:i:s');
        if (strtotime($fecha) > strtotime($fechaactual)) {
            $tutoria = [
                "nombre" => Input::get('name'),
                "descripcion" => Input::get('description'),
                "fecha" => $fecha . "T" . $hora,
                "lugar" => Input::get('place'),
                "capacidad" => Input::get('capacity'),
                "asignaturas_id" => Input::get('subject'),
                "tutores_id" => Auth::user()->tutor->id,

            ];
            $tutorial = Tutoria::create($tutoria);
            $new = [
                "id" => $tutorial->id,
                "name" => $tutorial->nombre,
                "description" => $tutorial->descripcion,
                //"created_at" => $tutorial->created_at,
                // "updated_at" => $tutorial->updated_at,
                "date" => $tutorial->fecha,
                "place" => $tutorial->lugar,
                "capacity" => $tutorial->capacidad,
                "tutors_id" => $tutorial->tutores_id,
                "subjects_id" => $tutorial->asignaturas_id,
                "state" => $tutorial->estado,
                "subjects_name" => $tutorial->asignatura->nombre,
                "tutor_name" => $tutorial->tutor->usuario->nombre,
                "users_id" => $tutorial->tutor->usuario->id
            ];
            if (is_array($new)) {
                $response->setError(false);
                $response->setData(["tutorial" => $new]);
                $response->setDescripcion("Se creo exitosamente la tutoria.");
            } else {
                $response->setError(true);
                $response->setDescripcion("No se pudo crear la tutoria");
            }
        } else {
            $response->setError(true);
            $response->setDescripcion("La fecha de la tutoria no puede ser anterior a la actual");
        }
        //return $response->getResponse();
        return Response::json($response->getResponse())->setCallback(Input::get('callback'));
        //  return json_encode($tutoriadevuelta);

    }

    public function eliminarTutoria()
    { // Metodo encargado de eliminar tutoria
        $response = new ResponseController();
        $response->setFechaInicial(microtime(true));
        $id = Input::get('id');
        if (!is_null($id)) {
            $tutoria = Tutoria::find($id);
            if (!is_null($tutoria)) {
                if ($tutoria->tutor->id == Auth::user()->tutor->id) {
                    foreach ($tutoria->usuarios as $usuario) { // Eliminamos los usuarios de la tutoria
                        //echo $usuario->nombre."<br>";
                        // A todos los usuarios inscritos se les debera enviar una notificacion al correo
                        $usuario->tutorias()->detach($id);
                    }
                    $tutoria->delete();
                    $response->setError(false);
                    $response->setDescripcion("Se elimino correctamente la tutoria");
                } else {
                    $response->setError(true);
                    $response->setDescripcion("Usted esta intentando borrar una tutoria que no le pertenece");
                }
            } else {
                $response->setError(true);
                $response->setDescripcion("No se encontro ninguna tutoria con el id especificado");
            }
        } else {
            $response->setError(false);
            $response->setDescripcion("El id ingresado no puede estar vacio");
        }
        return Response::json($response->getResponse())->setCallback(Input::get('callback'));
    }

    public function misAsignaturasFaltantes()
    {
        $asignaturas = Carrera::find(Auth::user()->carreras_id)->asignaturas;
        $array = [];
        if (!is_null(Auth::user()->tutor)) {
            $asignaturasvistas = Auth::user()->tutor->asignaturas;
            $i = 0;
            foreach ($asignaturas as $asignatura) {
                foreach ($asignaturasvistas as $asignaturasvistas2) {
                    if ($asignaturasvistas2->id == $asignatura->id) {
                        //array_splice($asignaturas, $i, 1);
                    }
                }
                //echo $asignatura->nombre."<br>";

            }
            $i += 1;
        }
        //var_dump($asignaturas);
        return ($asignaturas);
    }

    public function ingresarAsignaturas()
    {
        $responsecontroller = new ResponseController();
        $responsecontroller->setFechaInicial(microtime(true));
        $asignaturas = explode(',', Input::get("asignaturas"));
        // print_r($asignaturas);

        foreach ($asignaturas as $asignatura) {
            if (!is_null(Asignatura::find($asignatura))) {
                if (!is_object(Auth::user()->tutor)) {
                    $this->crearTutor();

                    // echo(Asignatura::find($asignatura)->nombre."<br>");
                }
                Auth::user()->tutor->asignaturas()->attach($asignatura);

            }
        }
        $responsecontroller->setError(false);
        $responsecontroller->setDescripcion("Asignaturas agregadas correctamente.");
        return $responsecontroller->getResponse();

    }
}