<?php
use Illuminate\Support\Facades\Auth;


/**
 * Created by Miguel Crespo.
 * Date: 26/03/14
 * Time: 04:38 PM
 */
class UsuarioController extends BaseController
{

    public function register($name, $email, $password, $password2)
    {

        $responsecontroller = new ResponseController();
        $responsecontroller->setFechaInicial(microtime(true));
        $responsecontroller->setError(true);

        $existe = Usuario::where("correo", "=", $email)->get();

        if (count($existe) == 0) {
            //$email_correcto = (bool)preg_match("/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/",$email);
            if (true !== false) {
                if (strlen($password) > 4 && $password == $password2) {

                    $user = [
                        "nombre" => $name,
                        "correo" => $email,
                        "password" => Hash::make($password),
                        "carreras_id" => 14
                    ];

                    $user2 = Usuario::create($user);

                    if (!empty($user2)) { // Comprobamos que este vacia, puede ser remplazada por is_object, is_array, etc
                        $responsecontroller->setError(false);
                        $responsecontroller->setDescripcion("Se registr&oacute; al usuario correctamente");
                    } else {
                        $responsecontroller->setDescripcion("Hubo un error al registrarse.");
                    }
                } else {
                    $responsecontroller->setDescripcion("La contrase&ntilde;a debe tener más de 4 caracteres.");
                }
            } else {
                $responsecontroller->setDescripcion("El email es invalido.");
            }
        } else {
            $responsecontroller->setDescripcion("Este correo ya se encuentra registrado");
        }

        return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));


    }

    public function login($email, $password)
    {

        $responsecontroller = new ResponseController();
        $responsecontroller->setFechaInicial(microtime(true));
        $responsecontroller->setError(true);

        //$email_correcto = (bool)preg_match("/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/",$email);
        if (true !== false) {
            if (strlen($password) > 4) {
                if (Auth::attempt(['correo' => $email, 'password' => $password])) {
                    $responsecontroller->setError(false);
                    $responsecontroller->setData(["user" => ["id" => Auth::user()->id, "name" => Auth::user()->nombre, "email" => Auth::user()->correo]]);
                } else {
                    $responsecontroller->setDescripcion("El usuario no existe o la contrase&ntilde;a es incorrecta");
                }
            } else {
                $responsecontroller->setDescripcion("La contrase&ntilde;a debe tener más de 4 caracteres.");
            }
        } else {
            $responsecontroller->setDescripcion("El email es invalido.");
        }
        return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));
    }


    public function inscribirseTutoria($idtutoria = null)
    {
        if ($idtutoria == null) {
            $idtutoria = Input::get("id");
        }
        $responsecontroller = new ResponseController();
        $responsecontroller->setFechaInicial(microtime(true));
        $tutorias = Auth::User()->tutorias;
        $encontro = false;
        $arrayusers = []; // new improve for update assistants list
        foreach ($tutorias as $tutoria2) {
            if ($tutoria2->id == Tutoria::find($idtutoria)->id) {
                $encontro = true;
                foreach ($tutoria2->usuarios as $user) {
                    if ($user->id == Auth::user()->id) {
                        $enrolled = true;
                    }
                    /* $user2 = [
                         "id" => $user->id,
                         "name" => $user->nombre
                     ];
                     array_push($arrayusers, $user2);*/
                }
            }
        }
        if ($encontro) {
            Auth::user()->tutorias()->detach($idtutoria);
            $responsecontroller->setError(false);

            foreach (Tutoria::find($idtutoria)->usuarios as $user) {
                if ($user->id == Auth::user()->id) {
                    $enrolled = true;
                }
                $user2 = [
                    "id" => $user->id,
                    "name" => $user->nombre
                ];
                array_push($arrayusers, $user2);
            }

            $responsecontroller->setData(["assistants" => $arrayusers]);
            $responsecontroller->setDescripcion("Se ha desiscrito exitosamente.");
            //return "true";
        } else {

            if (Auth::user()->id == Tutoria::find($idtutoria)->tutor->usuarios_id) {
                $responsecontroller->setError(true);
                $responsecontroller->setDescripcion("El tutor no puede inscribirse en su misma tutoria.");
            } else {

                $usuario = Auth::user();
                $usuario->tutorias()->attach($idtutoria);
                foreach (Tutoria::find($idtutoria)->usuarios as $user) {
                    if ($user->id == Auth::user()->id) {
                        $enrolled = true;
                    }
                    $user2 = [
                        "id" => $user->id,
                        "name" => $user->nombre
                    ];
                    array_push($arrayusers, $user2);
                }
                $responsecontroller->setData(["assistants" => $arrayusers]);
                $responsecontroller->setError(false);
                $responsecontroller->setDescripcion("La tutoria se ha agregado correctamente.");
            }
        }
        return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));

    }

    public function registrarseTutor()
    {
        if ((new TutorController())->soyTutor() == "false") {
            Tutor::create([
                "usuarios_id" => Auth::user()->id,
            ]);
            return "true";
        } else {
            return "false";
        }
    }

    public function misAsignaturasFaltantes()
    {
        $asignaturas = Carrera::find(Auth::user()->carrera->id)->asignaturas;
        return Response::json($asignaturas)->setCallback(Input::get('callback'));

    }

    public function ingresarAsignaturas()
    {
        $responsecontroller = new ResponseController();
        $responsecontroller->setFechaInicial(microtime(true));
        $asignaturas = explode(',', Input::get("asignaturas"));
        // print_r($asignaturas);

        foreach ($asignaturas as $asignatura) {
            if (!is_null(Asignatura::find($asignatura))) {
                Auth::user()->asignaturas()->attach($asignatura);
                // echo(Asignatura::find($asignatura)->nombre."<br>");
            }
        }
        $responsecontroller->setError(false);
        $responsecontroller->setDescripcion("Asignaturas agregadas correctamente.");
        //return $responsecontroller->getResponse();
        return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));


    }

    public function checkSession()
    {
        $responsecontroller = new ResponseController();
        $responsecontroller->setFechaInicial(microtime(true));
        if (Auth::check()) {
            $responsecontroller->setError(false);
            $responsecontroller->setDescripcion("User logged into Microdemia");
            $responsecontroller->setData(["user" => ["id" => Auth::user()->id, "name" => Auth::user()->nombre, "email" => Auth::user()->correo]]);
        } else {
            $responsecontroller->setError(true);
            $responsecontroller->setDescripcion("User did not log into Microdemia");
        }
        return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));
    }

    public function rateTutorial()
    {
        $responsecontroller = new ResponseController();
        $responsecontroller->setFechaInicial(microtime(true));
        $rate = Input::get("rate");
        $id = Input::get("id");

        $tutorial = Tutoria::find($id);
        if (is_object($tutorial)) {
            foreach ($tutorial->usuarios as $usuario) {
                if ($usuario->id == Auth::user()->id && $rate >= 1 && $rate <= 5) {
                    if ($usuario->pivot->calificacion != 0) {
                        $responsecontroller->setDescripcion("Ya califico esta tutoria");
                        $responsecontroller->setError(true);
                        return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));

                    }
                    $usuario->pivot->calificacion = $rate;
                    $usuario->pivot->save();
                    $responsecontroller->setDescripcion("Se califico exitosamente");
                    $responsecontroller->setError(false);
                    $responsecontroller->setData(["rate" => $rate]);
                    // echo($usuario->pivot->calificacion);
                    return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));

                }

            }
        } else {
            $responsecontroller->setDescripcion("No se encontro la tutoria");
            $responsecontroller->setError(true);
            return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));

        }
        $responsecontroller->setDescripcion("No se pudo calificar la tutoria");
        $responsecontroller->setError(true);
        return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));

    }

    public function infoUser()
    {

        $userinfo = [];
        $user = Auth::user();
        //$userinfo = $user->toArray();
        $information = [];

        $userinfo = [
            "id" => $user->id,
            "name" => $user->nombre,
            "email" => $user->correo,
            "description" => $user->descripcion,
            "created" => $user->created_at,
            "major" => $user->carrera->nombre
        ];
        $tutorialastutor = [];
        $subjectihave = [];
        $myrate = [];
        if (is_object(Auth::user()->tutor)) {

            //  $user["tutorialsastutor"] = Auth::user()->tutor->tutorias;
            // $userinfo["subjectsihave"] = Auth::user()->tutor->asignaturas->toArray();
            // $userinfo["tutorialsastutor"] = Auth::user()->tutor->tutorias->toArray();

            foreach (Auth::user()->tutor->tutorias as $tutorial) { //add all tutorials as tutor
                $rate = 0;
                $votes = 0;
                foreach ($tutorial->usuarios as $user) {
                    if ($user->pivot->calificacion != 0 && $tutorial->estado == 0) {
                        $rate += $user->pivot->calificacion;

                        $votes++;
                    }
                }
                $ratefinal = 0;
                $votesfinal = 0;
                if ($votes > 0) {
                    $ratefinal = $rate / $votes;
                    $votesfinal = $votes;
                }

                $tuto = [
                    "id" => $tutorial->id,
                    "name" => $tutorial->nombre,
                    "description" => $tutorial->descripcion,
                    "created_at" => $tutorial->created_at,
                    // "updated_at" => $tutorial->updated_at,
                    "date" => $tutorial->fecha,
                    "place" => $tutorial->lugar,
                    "capacity" => $tutorial->capacidad,
                    //"tutors_id" => $tutorial->tutores_id,
                    "subjects_id" => $tutorial->asignaturas_id,
                    "state" => $tutorial->estado,
                    "subjects_name" => $tutorial->asignatura->nombre,
                    "rate" => $ratefinal,
                    "votes" => $votesfinal,

                    "users_id" => $tutorial->tutor->usuario->id
                ];
                array_push($tutorialastutor, $tuto);
            }

            foreach (Auth::user()->tutor->asignaturas as $subject) {
                $sub = [
                    "id" => $subject->id,
                    "name" => $subject->nombre
                ];
                array_push($subjectihave, $sub);
            }


            $rate = 0;
            $votes = 0;
            foreach (Auth::user()->tutor->tutorias as $tutorial) {
                foreach ($tutorial->usuarios as $user) {
                    if ($user->pivot->calificacion != 0 && $tutorial->estado == 0) {
                        $rate += $user->pivot->calificacion;

                        $votes++;
                    }
                }

            }
            if ($votes > 0) {

                $myrate = [
                    "rate" => $rate / $votes,
                    "votes" => $votes
                ];
            } else {
                $myrate = [
                    "rate" => 0,
                    "votes" => 0
                ];
            }

        }

        // $userinfo["subjectsiwant"] = Auth::user()->asignaturas->toArray();

        $subjectiwant = [];
        foreach (Auth::user()->asignaturas as $subject) {
            $sub = [
                "id" => $subject->id,
                "name" => $subject->nombre
            ];
            array_push($subjectiwant, $sub);
        }

        // $userinfo["tutorialsasuser"] = Auth::user()->tutorias->toArray();
        $tutorialasuser = [];
        foreach (Auth::user()->tutorias as $tutorial) { //add all tutorials as tutor
            $tuto = [
                "id" => $tutorial->id,
                "name" => $tutorial->nombre,
                "description" => $tutorial->descripcion,
                "created_at" => $tutorial->created_at,
                // "updated_at" => $tutorial->updated_at,
                "date" => $tutorial->fecha,
                "place" => $tutorial->lugar,
                "capacity" => $tutorial->capacidad,
                "tutors_id" => $tutorial->tutores_id,
                "subjects_id" => $tutorial->asignaturas_id,
                "state" => $tutorial->estado,
                "subjects_name" => $tutorial->asignatura->nombre,
                "tutor_name" => $tutorial->tutor->usuario->nombre,
                "enrolled_date" => $tutorial->pivot->created_at,
                "users_id" => $tutorial->tutor->usuario->id
                // "assistants" => $assistants,
                //"users_id" => $tutorial->tutor->usuario->id,
                // "enrolled" => $enrolled
            ];
            array_push($tutorialasuser, $tuto);
        }

        $information = [
            "user" => $userinfo,
            "rate" => $myrate,
            "tutorialsastutor" => $tutorialastutor,
            "tutorialsasuser" => $tutorialasuser,
            "subjectsiwant" => $subjectiwant,
            "subjectsihave" => $subjectihave

        ];

        // $user["tutorialsasuser"] = Auth::user()->tutorias;
        return Response::json($information)->setCallback(Input::get('callback'));

    }
}
