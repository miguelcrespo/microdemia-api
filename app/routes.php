<?php
Route::get('/', function () {
    return View::make('index');
});

Route::get('register', function () {
    $parametros = Input::all();
    $usercontroller = new UsuarioController();
    return $usercontroller->register($parametros["name"], $parametros["email"], $parametros["password"], $parametros["password2"]);
});


Route::any('loginapp', function () {
    $parametros = Input::all();
    $usercontroller = new UsuarioController();
    return $usercontroller->login($parametros["email"], $parametros["password"]);
});


Route::any('login', function () {
    $response = new ResponseController();
    $response->setFechaInicial(microtime(true));
    $response->setError(true);
    $response->setDescripcion("you are not logged into Microdemia");
    return Response::json($response->getResponse())->setCallback(Input::get('callback'));
});

Route::get('checksession', array('uses' => 'UsuarioController@checkSession')); // Check if exist a session started 

Route::group(array('before' => 'auth', 'prefix' => 'api'), function () { // Rutas pertenecientes a la API de Microdemia
    //MODULO TUTOR

    Route::any('soytutor', array('uses' => 'TutorController@soyTutor')); // verificar si el usuario es un tutor

    Route::any('registrarmetutor', array('uses' => 'UsuarioController@registrarseTutor')); // registrarse para poder ser tutor
    Route::any('todasmistutorias', array('uses' => 'TutorController@todasmisTutorias')); //obtener todas las tutorias que dicto
    Route::any('misasignaturas', array('uses' => 'TutorController@misAsignaturas')); //obtener asignaturas que puedo dictar
    Route::any('mistutoriaspendientes', array('uses' => 'TutorController@misTutoriasPendientes')); //obtener tutorias que tengo pendientes por dictar
    Route::any('mistutoriasrealizadas', array('uses' => 'TutorController@misTutoriasRealizadas')); //obtener tutorias que he realizado
    Route::any('creartutoria', array('uses' => 'TutorController@crearTutoria')); //crear una tutoria
    Route::any('eliminartutoria', array('uses' => 'TutorController@eliminarTutoria')); //crear una tutoria
    Route::any('obtenerasignaturas', array('uses' => 'UsuarioController@misAsignaturasFaltantes')); //Obtener las asignaturas que puedo recibir
    Route::any('ingresarasignaturas', array('uses' => 'UsuarioController@ingresarAsignaturas')); // Ingresar las asignaturas que quiero recibir en el sistema
    Route::any('ingresarasignaturastutorias', array('uses' => 'TutorController@ingresarAsignaturas')); // Ingresar las asignaturas que puedo dictar

    Route::any('gettutorials', array('uses' => 'TutoriaController@getTutorials')); // Function to get tutorials that is in present and that i should want
    Route::any('findtutorial', array('uses' => 'TutoriaController@findTutorial')); // Function to get tutorials that is in present and that i should want
    Route::any('disabledtutorials', array('uses' => 'TutoriaController@disabledTutorials')); // Function to get tutorials that is in present and that i should want
    Route::any('ratetutorial', array('uses' => 'UsuarioController@rateTutorial')); // Function to get tutorials that is in present and that i should want
    Route::any('userinfo', array('uses' => 'UsuarioController@infoUser')); // Function to get tutorials that is in present and that i should want
    Route::any('tutorialsbysubject', array('uses' => 'TutoriaController@getTutorialsBySubject')); // Function to get tutorials that is in present and that i should want

    Route::any('logout', function () { // API para desloguearse en el sistema
        $response = new ResponseController();
        $response->setFechaInicial(microtime(true));
        Auth::logout();
        if (!Auth::check()) {
            $response->setError(false);
            $response->setDescripcion("Sesion cerrada con exito");

        } else {
            $response->setError(true);
            $response->setDescripcion("Fallo en el cierre de session");
        }
        return Response::json($response->getResponse())->setCallback(Input::get('callback'));
    });
    //---------------------------------------

    // MODULO USUARIO
    Route::any('inscribirsetutoria', array('uses' => 'UsuarioController@inscribirseTutoria')); // Todas las tutorias dentro del sistema

});


